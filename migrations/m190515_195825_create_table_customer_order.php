<?php

use yii\db\Migration;

class m190515_195825_create_table_customer_order extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%customer_order}}', [
            'id' => $this->integer(),
            'customer_id' => $this->integer(),
            'order_id' => $this->integer(),
            'product_id' => $this->integer(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%customer_order}}');
    }
}
