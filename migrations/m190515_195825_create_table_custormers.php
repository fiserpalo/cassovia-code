<?php

use yii\db\Migration;

class m190515_195825_create_table_custormers extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%custormers}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'ico' => $this->string(),
            'address' => $this->string(),
            'contact_person' => $this->string(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%custormers}}');
    }
}
