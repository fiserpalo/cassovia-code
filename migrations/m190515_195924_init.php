<?php

use app\models\User;
use yii\db\Migration;

/**
 * Class m190515_195924_init
 */
class m190515_195924_init extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $user = new User();
        $user->username = "admin";
        $user->password = "s3cur3";
        $user->email = "test@test.com";
        $user->role = "ADMIN";
        $user->save();
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190515_195924_init cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190515_195924_init cannot be reverted.\n";

        return false;
    }
    */
}
