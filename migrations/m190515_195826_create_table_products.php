<?php

use yii\db\Migration;

class m190515_195826_create_table_products extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'ean' => $this->string(),
            'picture' => $this->string(),
            'description' => $this->string(),
            'price' => $this->decimal(),
        ], $tableOptions);

    }

    public function down()
    {
        $this->dropTable('{{%products}}');
    }
}
