<?php
use yii\helpers\Html;
/* @var $reportData app\models\UserLog */
/* @var $userData app\models\User */
?>
<div>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">User</th>
            <th scope="col">Action</th>
            <th scope="col">Date</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($reportData as $report) { ?>
            <tr>
                <td><?php echo $report['username'] ?></td>
                <td><?php echo $report['action'] ?></td>
                <td><?php echo $report['created_on'] ?></td>
            </tr>

        <?php } ?>
        </tbody>
    </table>

</div>
