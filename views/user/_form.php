<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>


    <!--    <div class="form-group field-user-username required has-success">-->
    <!--        <label class="control-label" for="user-username">Username</label>-->
    <!--        <input type="text" id="user-username" class="form-control" name="User[username]" maxlength="100" aria-required="true" autocomplete="off" style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR4nGP6zwAAAgcBApocMXEAAAAASUVORK5CYII=&quot;); cursor: auto;" aria-invalid="false">-->
    <!---->
    <!--        <div class="help-block"></div>-->
    <!--    </div>-->


    <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
    function validateUsername() {
        var username = document.getElementById("user-username").value;
        var xhttp = new XMLHttpRequest();

        $.ajax({
            url: '<?php echo Yii::$app->request->baseUrl . '/index.php/user/check' ?>',
            type: 'post',
            data: {username: $("#user-username").val()},
            success: function (data) {
                var usernameElement = $("#user-username").parent()[0];
                if (data['username']) {
                    usernameElement.classList.add("has-error");
                    usernameElement.children[2].innerHTML = "Užívateľské meno sa už používa";
                } else {
                    usernameElement.classList.remove("has-error");
                    usernameElement.children[2].innerHTML = "";
                }

            }

        });
    }

    document.getElementById("user-username").addEventListener("focusout", validateUsername)


</script>