<?php


/* @var $products */
/* @var $customers */
/* @var $cart */
/* @var $this yii\web\View */
/* @var $model app\models\Custormers */

/* @var $form yii\widgets\ActiveForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div>

    <br>
    <form>
        <table>
            <tr>
                <td>
                    Customer
                </td>
                <td>
                    <select name="customers">
                        <option value="new"> New Customer</option>
                        <?php

                        foreach ($customers as $customer) {
                            echo '<option value="' . Html::encode($customer->id) . '">' . Html::encode($customer->name) . '</option>';
                        }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>
                    <button type="button" id="products">Add Product</button>
                </td>
            </tr>
            <tr class="product">
                <td>
                    Product
                </td>
                <td>
                    <select class="item" onchange="calculatePrice()">

                        <?php

                        foreach ($products as $product) {
                            echo '<option data-price="'.Html::encode($product->price).'" name="product" value="' . Html::encode($product->id) . '">' . Html::encode($product->name) . '</option>';
                        }
                        ?>
                    </select>
                </td>
                <td>Quantity<input name="quantity" class="quantity" value="0" type="number" onchange="calculatePrice()"></td>
                <td>Price <input name="finalPrice" class="finalPrice" disabled value="0"> </td>
            </tr>

    </form>
    </table>
</div>


<script>
    function calculatePrice() {


        for (var product of document.getElementsByClassName("product"))
        {
            var select = product.querySelector(".item");
            var price = select.options[select.selectedIndex].dataset.price;


            product.querySelector(".finalPrice").value = product.querySelector(".quantity").value * price;
        }

    }

    function addProduct() {
        var row = document.getElementsByName("product")[0].closest("tr");
        var table = row.closest("tbody");
        var clone = row.cloneNode(true);
        table.appendChild(clone);
    }

    document.getElementById("products").addEventListener("click", addProduct);


</script>