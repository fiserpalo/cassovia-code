<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "users_log".
 *
 * @property int $id
 * @property int $user_id
 * @property string $action
 * @property string $created_on
 */
class UserLog extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id'], 'integer'],
            [['created_on'], 'safe'],
            [['action'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'action' => 'Action',
            'created_on' => 'Created On',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getReportData()
    {
        $data = array();

        foreach (self::find()->all() as $log) {
            $data[] = [
                'action' => $log['action'],
                'created_on' => $log['created_on'],
                'username' => User::findOne(['id' => $log['user_id']])->username
            ];


        }

        return $data;
    }

}
