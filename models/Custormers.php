<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "custormers".
 *
 * @property int $id
 * @property string $name
 * @property string $ico
 * @property string $address
 * @property string $contact_person
 */
class Custormers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'custormers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'ico', 'address', 'contact_person'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'ico' => 'Ico',
            'address' => 'Address',
            'contact_person' => 'Contact Person',
        ];
    }
}
